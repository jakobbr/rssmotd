# rssmotd
*...i'm sure this type of software has never been written before!*

## how?

```sh
python3 -m pip install -r requirements.txt
echo "0.0" > ~/.config/rssmotd.lastexec
$EDITOR ~/.config/rssmotd.lst
```

### `rssmotd.lst` format

It's a newline-delimited list of feed URLs.

You can customize titles by adding `#Title`.
Comment out by adding `#`

Example:
```
http://www.archlinux.org/feeds/news/#Arch Linux News
http://xkcd.com/rss.xml#XKCD
#http://super-boring-rss-feed.example.com/yawn.xml
```

### advanced setup

I have the script symlinked to `~/bin` (which is in my PATH) and the
following is part of my .zshrc:

```sh
if [ "$TERM" = "linux" ]; then
    ~/bin/motd
fi
```

This gives me the MOTD in the terminal before starting X.

## photo

Look!

![photo.png](https://i.imgur.com/d3s9v2L.png)


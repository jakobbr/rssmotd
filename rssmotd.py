#!/usr/bin/env python3

import os
import feedparser
import time
from termcolor import colored, cprint

LASTEXEC_PATH = "~/.config/rssmotd.lastexec"
NOTE_PATH = "~/.motd"

def printfeed(url, lastexec):
    if url.startswith("#"):
        return

    cprint("%s" % url.split("#")[-1], "cyan", attrs=["bold"])
    feed = feedparser.parse(url)
    for item in feed["items"]:

        if "published_parsed" in item.keys():
            publ_stamp = time.mktime(item["published_parsed"])
        elif "updated_parsed" in item.keys():
            publ_stamp = time.mktime(item["updated_parsed"])
        else:
            publ_stamp = 253402210800 # The Future!

        if(publ_stamp + 3600 * 48 <= lastexec):
            continue

        publ = time.strftime("%F", time.localtime(publ_stamp))
        title = item["title"]

        cprint(" [%s]" % publ, "cyan", end="")
        cprint(" %s" % title, "blue", attrs=["bold"])
        cprint("   %s" % item["link"])
    print()
    return

def main():
    with open(os.path.expanduser("~/.config/rssmotd.lst"), "r") as f:
        feeds = f.read().splitlines()
    with open(os.path.expanduser(LASTEXEC_PATH), "r") as f:
        lastexec = float(f.read())

    [printfeed(url, lastexec) for url in feeds]

    with open(os.path.expanduser(LASTEXEC_PATH), "w+") as f:
        f.write(str(time.time()))

    notes = os.path.expanduser(NOTE_PATH)
    if os.path.exists(notes):
        print("-" * 72)
        with open(notes, "r") as f:
            print(f.read())

if __name__ == "__main__":
    main()
